# Modules
import os
import csv

# Name file path
bank_file = os.path.join('Resources','budget_data.csv')

# Open file and identify header
with open(bank_file) as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")

    # Set headers
    csv_header = next(csvreader)

    # Part 1: List to hold unique months
    month_list = []
    
    # Part 2: Hold total sum
    total_sum = 0
    
    # Part 3: Hold for changes
    last_month = str("N/A")
    this_month = 0
    change = 0
    total_change = 0

    # Part 4: Hold for biggest and smallest change
    change_list = []
    
    # Check data by row
    for row in csvreader:

        #Part 1: Get number of months in dataset
        #Reset duplicate counter
        duplicate_check = 0

        # Loop through list of months to check for duplicates
        for i in month_list:

            # Each time a duplicate month is hit, add 1 to counter
            if (i == row[0]):
                duplicate_check = duplicate_check + 1

        # If counter is 0, there is no duplicate and can be added to list 
        if (duplicate_check == 0):
            month_list.append(row[0])

        # Count number of unique months in list
        month_count = len(month_list)

        #Part 2: Sum of profit/losses
        total_sum = total_sum + int(row[1])

        #Part 3: Average of changes
        this_month = int(row[1])
        if (last_month != str("N/A")):
                change = this_month - last_month
                total_change = total_change + change
        last_month = int(this_month)

        #Part 4 & 5: Biggest & lowest profit or loss
        change_list.append(change)
                

# Print summary info
print("\n-------------------------------------")

print("Financial Analysis")

# Print part 1
print(f"Months: {month_count}")

# Print part 2
print(f"Sum: ${total_sum}")

# Calculate and print part 3
average_change = round(total_change / (month_count - 1),2)
print(f"Average change: ${average_change}")

# Calculate and print part 4
big_profit = max(change_list)
list_place_profit = change_list.index(big_profit)
print(f"Greatest Increase in Profits: {month_list[list_place_profit]} ${big_profit}")

# Calculate and print part 5
big_loss = min(change_list)
list_place_loss = change_list.index(big_loss)
print(f"Greatest Decrease in Losses: {month_list[list_place_loss]} ${big_loss}")

print("-------------------------------------\n")

# Write txt file

txt_file = open("Analysis/financial_analysis_summary.txt", "w")
txt_file.write("-------------------------------------\n")
txt_file.write("Financial Analysis\n")
txt_file.write(f"Months: {month_count}\n")
txt_file.write(f"Sum: ${total_sum}\n")
txt_file.write(f"Average change: ${average_change}\n")
txt_file.write(f"Greatest Increase in Profits: {month_list[list_place_profit]} ${big_profit}\n")
txt_file.write(f"Greatest Decrease in Losses: {month_list[list_place_loss]} ${big_loss}\n")
txt_file.write("-------------------------------------\n")