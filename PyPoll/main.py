# Modules
import os
import csv

# Name file path
poll_file = os.path.join('Resources','election_data.csv')

# List to hold unique voter IDs
id_list = []

# Candidate list
candidate_list = []
# Unique candidate list
ucandidate_list = []

# Winner
winner = []
candidate_votes = 0

# Open file and identify header
with open(poll_file) as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")

    # Set headers
    csv_header = next(csvreader)

    # Check data by row
    for row in csvreader:

        # Get number of votes in dataset
        id_list.append(row[0])

        # List of candidates
        candidate_list.append(row[2])

        # Get list of unique candidates
        if (ucandidate_list.count(row[2]) == 0):
            ucandidate_list.append(row[2])

# Print election results        
print("\nElection Results\n--------------------------")

# Print total votes
print(f"Total Votes: {len(id_list)}")

print("--------------------------")

# Analyze and print data for each candidate individually
for candidate in ucandidate_list:
    vote_count = candidate_list.count(candidate)
    total_count = len(id_list)
    vote_percent = round(vote_count / total_count * 100,3)
    print(f"{candidate}: {vote_percent}% ({vote_count})")

    # Winner check
    if (int(vote_count) > int(candidate_votes)):
        winner.append(candidate)

print("--------------------------")

# Print election winner
print(f"Winner: {winner[0]}")

print("--------------------------")

# Write txt file

txt_file = open("Analysis/election_results.txt", "w")
txt_file.write("\nElection Results\n--------------------------\n")
txt_file.write(f"Total Votes: {len(id_list)}\n")
txt_file.write("--------------------------\n")
for candidate in ucandidate_list:
    vote_count = candidate_list.count(candidate)
    total_count = len(id_list)
    vote_percent = round(vote_count / total_count * 100,3)
    txt_file.write(f"{candidate}: {vote_percent}% ({vote_count})\n")
txt_file.write("--------------------------\n")
txt_file.write(f"Winner: {winner[0]}\n")
txt_file.write("--------------------------\n")